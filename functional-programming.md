# 関数型プログラミング勉強会

- 関数型プログラミング in javascript

  [https://www.slideshare.net/ryumatsukano/in-javascript]()

- JavaScriptで関数型プログラミングの入門

  [http://qiita.com/takeharu/items/cf98d352ff574c5ac536]()

- 参照透過性とは何だ？

  [http://hamuhamu.hatenablog.jp/entry/2015/04/04/235148]()

- Functional JavaScript

  [https://gist.github.com/ympbyc/5564146]()

- 関数型プログラミングはまず考え方から理解しよう

  [http://qiita.com/stkdev/items/5c021d4e5d54d56b927c]()

- Map / Reduce in ES5

  [https://www.webprofessional.jp/map-reduce-functional-javascript/]()

- PHP における関数型プログラミング

  [http://ja.phptherightway.com/pages/Functional-Programming.html]()

- 第一級関数（ファーストクラスファンクション） is 何？となったので調べてみた

  [http://syossan.hateblo.jp/entry/2016/07/16/211248]()

- Scala の関数

  [http://tkawachi.github.io/blog/2014/11/26/1/]()
